import React from "react"

export default function StartScreen(props) {
    
    return (
        <div>
            <h1 className="title">Quizzical</h1>
            <p className="credits">Made by Rein Brobbel</p>
            <button className="startButton" onClick={props.onClick}>Start Quiz</button>
        </div>
    )
}