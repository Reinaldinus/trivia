import React from "react"
import { nanoid } from 'nanoid'
import Answerbutton from './components/Answerbutton'
import shuffleArray from "./shuffleArray"
import Confetti from 'react-confetti'


export default function QuizPage() {
    
    const [data, setData] = React.useState({})


    React.useEffect(() => {
        fetch("https://opentdb.com/api.php?amount=10")
            .then((response) => response.json())
            .then((data => {

                const expandedDataArray = data.results.map(question => {
            //        console.log(question.correct_answer)

                    if (question.type.includes("multiple") ) {

                    return (
                        {
                            ...question,
                            answers: shuffleArray([...question.incorrect_answers, question.correct_answer])
                        }
                    )} else return ({
                        ...question,
                        answers: [...question.incorrect_answers, question.correct_answer]
                    })
                })

                const expandedAnswerArray = expandedDataArray.map(question => {
                    return {...question, 
                            answers: question.answers.map(oldAnswer => (
                                {
                                    id: nanoid(), 
                                    answer: oldAnswer, 
                                    clicked: false,
                                    correct: oldAnswer === question.correct_answer? true : false,
                                    checked: false
                        }))}
                })
                setData(expandedAnswerArray)
            }))
        }, [])
         
    // Reloads data for new questions
    function restartGame() {
        fetch("https://opentdb.com/api.php?amount=10")
        .then((response) => response.json())
        .then((data => {
                
            const expandedDataArray = data.results.map(question => {
                return (
                    {
                        ...question,
                        answers: shuffleArray([...question.incorrect_answers, question.correct_answer])
                    }
                )
            })
    
            const expandedAnswerArray = expandedDataArray.map(question => {
                return {...question, 
                        answers: question.answers.map(oldAnswer => (
                            {
                                id: nanoid(), 
                                answer: oldAnswer, 
                                clicked: false,
                                correct: oldAnswer === question.correct_answer? true : false,
                                checked: false
                    }))}
            })
            setData(expandedAnswerArray)
            
        }))
    }

    // Toggles clicked property in Answerbuttons
    function toggleClicked(id, answersFromClickedQuestion, howManyCorrect, howManyInCorrect) {

        // Checks if answerbuttons have already been checked. If so toggleClicked does not work
        if (!howManyCorrect > 0 || !howManyInCorrect > 0) {

            // Returns new data for questions, with a clicked answer corresponding to id
            setData(questions => {
                const newQuestions = questions.map(oldQuestion => {
                    if (oldQuestion.answers === answersFromClickedQuestion){
                        const newAnswers = oldQuestion.answers.map(oldAnswer => {
                            if (oldAnswer.id === id) {return ({...oldAnswer, clicked: !oldAnswer.clicked})}
                            else return ({...oldAnswer, clicked: false})
                        })
                        return ({...oldQuestion, answers: newAnswers})
                    } else return oldQuestion
                }) 
                return newQuestions
        })}
    }

    let howManyClicked = 0

    // Sets all Answerbutton 'checked' property from false to true
    function checkAnswers() {

        // Checks if all buttons are clicked
        if (howManyClicked === 10 ) {
            setData(questions => {
                const checkedQuestions = questions.map(oldQuestion => {
                    const checkedAnswers = oldQuestion.answers.map(oldAnswer => {
                        return ({...oldAnswer, checked: true})
                    })
                    return ({...oldQuestion, answers: checkedAnswers})
                })
                return checkedQuestions
        })}
    }

    // Checks if api has loaded
    if (data.length > 0) {

        let howManyCorrect = 0
        let howManyInCorrect = 0
        

        // Returns all question elements, including Answerbuttons
        const questionElements = data.map(question => {

            
            let textCorrectedQuestion = question.question.replace(/&quot;glass&quot;/g, "'")
            textCorrectedQuestion = textCorrectedQuestion.replace(/&quot;/g, "'")
            textCorrectedQuestion = textCorrectedQuestion.replace(/&#039;/g, "'")
            textCorrectedQuestion = textCorrectedQuestion.replace(/&deg;/g, "°")
            textCorrectedQuestion = textCorrectedQuestion.replace(/&amp;/g, "&")

            const buttonElements = question.answers.map(answer => {
                
                // Checks if Answerbutton is either held, not held, held and correct or held and incorrect
                let style = "notHeld"
                if (answer.clicked && !answer.checked) {
                    style = "held"
                    howManyClicked++

                } else if (answer.clicked && answer.checked && answer.correct) {
                    style = "checkedAndCorrect"
                    howManyCorrect++
                    
                } else if (!answer.clicked && answer.checked && answer.correct) {
                    style = "unclickedAndCorrect"

                } else if (answer.clicked && answer.checked) {
                    style = "checkedAndIncorrect"
                    howManyInCorrect++
                } 

                // Returns Answerbutton component
                return (
                    <Answerbutton 
                        answer={answer}
                        clicked={answer.clicked}
                        onClick={() => toggleClicked(answer.id, question.answers, howManyCorrect, howManyInCorrect)}
                        className={style}
                    />
                )
            })
            
            const totalQuestionKey = nanoid()
            return (
                <div key={totalQuestionKey}>
                    <p className="question__question">{textCorrectedQuestion}</p>
                    {buttonElements}
                </div>
            )
        })

        return (
            <div>
                {(howManyCorrect > 9) && <Confetti height={1000}/>}
                {questionElements}
                <button className="checkButton" onClick={() => checkAnswers()}>Check answers</button>
                {
                    (howManyCorrect > 0 || howManyInCorrect > 0) 
                    && 
                    <h1 className="correctCounter">You have {howManyCorrect} correct answers</h1>
                }

                {
                    (howManyCorrect > 0 || howManyInCorrect > 0) 
                    && 
                    <button className="replayButton" onClick={() => restartGame()}>Replay</button>
                }
            </div>
        )
    }      
}       