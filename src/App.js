import React from "react";
import StartScreen from "./StartScreen";
import QuizPage from "./QuizPage";

import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBAFRCBlIEgeQBN2PFx0YOGEvMjKqVbRec",
  authDomain: "trivia-d9f55.firebaseapp.com",
  projectId: "trivia-d9f55",
  storageBucket: "trivia-d9f55.appspot.com",
  messagingSenderId: "221275109813",
  appId: "1:221275109813:web:ac4ddbcf15a56082fc81fc",
};

initializeApp(firebaseConfig);

export default function App() {
  const [gameStarted, setGameStarted] = React.useState(false);

  function startGame() {
    setGameStarted(true);
  }

  return (
    <div className="container">
      {!gameStarted && <StartScreen onClick={startGame} />}
      {gameStarted && <QuizPage />}
    </div>
  );
}
