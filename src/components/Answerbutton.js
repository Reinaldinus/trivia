import React from "react"

export default function Answerbutton(props) {


    let textCorrectedAnswer = props.answer.answer.replace(/&quot;glass&quot;/g, "'")
    textCorrectedAnswer = textCorrectedAnswer.replace(/&quot;/g, "'")
    textCorrectedAnswer = textCorrectedAnswer.replace(/&#039;/g, "'")
    textCorrectedAnswer = textCorrectedAnswer.replace(/&deg;/g, "°")
    textCorrectedAnswer = textCorrectedAnswer.replace(/&amp;/g, "&")


    return (
        <div className="question__buttons">
            <button
                className={props.className}
                onClick={props.onClick}
            >
            {textCorrectedAnswer}
            </button>
        </div>
    )
}